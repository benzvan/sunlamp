// Simple NeoPixel test.  Lights just a few pixels at a time so a
// 1m strip can safely be powered from Arduino 5V pin.  Arduino
// may nonetheless hiccup when LEDs are first connected and not
// accept code.  So upload code first, unplug USB, connect pixels
// to GND FIRST, then +5V and digital pin 6, then re-plug USB.
// A working strip will show a few pixels moving down the line,
// cycling between red, green and blue.  If you get no response,
// might be connected to wrong end of strip (the end wires, if
// any, are no indication -- look instead for the data direction
// arrows printed on the strip).

#include <Adafruit_NeoPixel.h>

#define PIN    5
#define N_LEDS 150

Adafruit_NeoPixel strip = Adafruit_NeoPixel(N_LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  strip.begin();

  strip.show();
  delay(50);

  for(uint16_t k=0; k<5; k++) {
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
  }
}

void loop() {

 // wheel

  for(uint16_t j=0; j<256; j=j+4) {
    for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(25);
  }
  

/*
    setStrip(strip.Color(255,0,0));
    strip.show();
    delay(1000);
    clear();
    strip.show();
    delay(1000);
  
/*
  for (i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(128, 128, 128));
  }
  strip.show();

  delay(1000);

  for (i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(255, 255, 255));
  }
  strip.show();
  delay(1000);
  */

  /*
    for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(j, j, j));
    }
    strip.show();
    delay(25);
    }
    for(j=255; j<-1; j--) {
    for(i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(j, j, j));
    }
    strip.show();
    delay(25);
    }
  */
  /*
    chase(strip.Color(255, 0, 0)); // Red
    chase(strip.Color(0, 255, 0)); // Green
    chase(strip.Color(0, 0, 255)); // Blue
    chase(strip.Color(255, 255, 255)); // White
  */

}

static void clear() {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0,0,0));
  }
}

static void setStrip(uint32_t c) {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
  }
}

static void setLongSpiral(uint16_t s, uint32_t c) {
  for (uint16_t i = s; i = strip.numPixels(); i = i + 3) {
    strip.setPixelColor(i, c);
  }
  strip.show();
}

static void chase(uint32_t c) {
  for (uint16_t i = 0; i < strip.numPixels() + 75; i++) {
    strip.setPixelColor(i  , c); // Draw new pixel
    strip.setPixelColor(i - 75, 0); // Erase pixel a few steps back
    strip.show();
    delay(25);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  else if (WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  else {
    WheelPos -= 170;
    return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}
