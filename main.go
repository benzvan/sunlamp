package sunlamp

import (
	"machine"
	"time"

	//"tinygo.org/x/drivers/ws2812" // neopixels
	//"tinygo.org/x/drivers/sd1306" // oled
)


func main() {
    led := machine.LED
    led.Configure(machine.PinConfig{Mode: machine.PinOutput})
    for {
        led.Low()
        time.Sleep(time.Millisecond * 1000)

        led.High()
        time.Sleep(time.Millisecond * 1000)
    }
}