# sunlamp

A custom lamp using arduino, tinygo, and neopixels, with a rotary encoder and OLED display

### Pinout

#### Spark Photon

| pin | Use | Notes
|-----|-----|----
| Bottom row (USB on left)
| ViN | 5v power | connect through diode
| GND | connect to GND bus
| TX  |
| RX  |
| WKP | | Wakeup
| DAC | | 12-bit DAC
| A5  |     | SPI MOSI
| A4  |     | SPI MISO
| A3  |     | SPI SCK
| A2  |     | SPI SS
| A1  |     |
| A0  | Neopixel data    |
||
| Top row (USB on left)
| 3V3 | 3.3v out
| RST | Reset
| VBAT | backup power to RTC
| GND | connect to GND bus
| D7  | Rotary Encoder button
| D6  | Rotary Encoder A
| D5  | Rotary Encoder B
| D4  | OLED Button A
| D3  | OLED Button B
| D2  | OLED Button C
| D1  | I2C SCL
| D0  | I2C SDA

#### Adafruit I2C OLED Display

I2C Address 0x3C

| Pin | Use | Notes
|-----|-----|--
| Bottom Row (buttons on left)
| 0   | Reset | connect to reset pin
| 1   | 3V  | Connect to 3.3v out
| 2   | 
| 3   | GND | Connect to GND bus
| Top Row (Buttons on left)
| A   | Button A | D4
| B   | Button B | D3
| C   | Button C | D2
|     | I2C SCL  | D1
|     | I2C SDA  | D0